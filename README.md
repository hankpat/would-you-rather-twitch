# Tolson

=================

This app consists of two main parts.

1. (`./src`) A frontend that is compiled into the twitch extension
2. (`./services`) A backend node project that exposes an express server for a connnecting client, a realtime IRC subscription to obtain chat messages, and a mechanism to broadcast changes to all the clients.

## Getting Started

### Frontend

The frontend can be run as a webpack dev server when working locally.

```
npm i
npm run client
```

Or it can be built into a bundle using

```
npm run build
```

### Backend

The backend is hosted on glitch, and can be worked on while syncing to it's deployed state.

> NOTE: This extension is pretty experimental at this stage, and as such glitch allows for rapid iterations. It should be known that any changes made on the master branch will be effective as soon as pushed to the glitch remote. If you edit directly in the glitch console, or using the [VS Code glitch extension](https://marketplace.visualstudio.com/items?itemName=glitch.glitch)

### Docker

> Note: You might need to authenticate with gcloud if you haven't already `gcloud auth configure-docker`

#### Build the image

```
docker build . -t tolson-api
```

#### Tag + push the image to gcr

```
docker tag tolson-api gcr.io/twitch-tolson-271116/tolson-api
docker push gcr.io/twitch-tolson-271116/tolson-api
```

#### Deploy to k8s

```
k apply -f ./k8s/tolson.yaml
```
