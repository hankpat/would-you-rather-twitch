const path = require("path")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const { CleanWebpackPlugin } = require("clean-webpack-plugin")
const webpack = require("webpack")
const dotenv = require("dotenv")

module.exports = (_env, argv) => {
  return {
    mode: "development",
    entry: {
      video_overlay: "./src/video_overlay/index.js",
      live_config: "./src/live_config/index.js",
      config: "./src/config/index.js",
    },
    devtool: "inline-source-map",
    devServer: {
      contentBase: "./public",
      host: argv.devrig ? "localhost.rig.twitch.tv" : "localhost",
      headers: {
        "Access-Control-Allow-Origin": "*",
      },
      port: 8080,
      https: true,
    },
    plugins: [
      new CleanWebpackPlugin(),
      new HtmlWebpackPlugin({
        title: "Video Overlay",
        filename: "video_overlay.html",
        chunks: ["video_overlay"],
        template: "./src/video_overlay/index.html",
      }),
      new HtmlWebpackPlugin({
        title: "Live Configuration",
        filename: "live_config.html",
        chunks: ["live_config"],
        template: "./src/live_config/index.html",
      }),
      new HtmlWebpackPlugin({
        title: "Config",
        filename: "config.html",
        chunks: ["config"],
        template: "./src/config/index.html",
      }),
      new webpack.DefinePlugin({
        "process.env": {
          API_URL: JSON.stringify(dotenv.config().parsed.API_URL),
        },
      }),
    ],
    output: {
      filename: "[name].bundle.js",
      path: path.resolve(__dirname, "dist"),
    },
    resolve: {
      alias: {
        svelte: path.resolve("node_modules", "svelte"),
      },
      extensions: [".mjs", ".js", ".svelte"],
      mainFields: ["svelte", "browser", "module", "main"],
    },
    module: {
      rules: [
        {
          test: /\.svelte$/,
          use: {
            loader: "svelte-loader",
            options: {
              emitCss: true,
              hotReload: true,
            },
          },
        },
        {
          test: /\.css$/,
          use: ["style-loader", "css-loader"],
          exclude: /node_modules/,
        },
      ],
    },
  }
}
