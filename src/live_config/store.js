import { writable, derived } from "svelte/store"
import { nanoid } from "nanoid"

const generateDilemma = () => {
  return { id: nanoid(), question: "", leftPosition: { id: nanoid(), text: "" }, rightPosition: { id: nanoid(), text: "" } }
}

const createDilemmas = () => {
  const { subscribe, set, update } = writable({
    currentDilemmaIndex: 0,
    values: [generateDilemma()],
  })
  return {
    subscribe,
    add: () => update((d) => ({ ...d, values: [...d.values, generateDilemma()] })),
    remove: (id) => update((d) => ({ ...d, values: [...d.values.filter((i) => i.id !== id)] })),
    clear: () => set({ currentDilemmaIndex: 0, values: [generateDilemma()] }),
    next: () => update((d) => ({ ...d, currentDilemmaIndex: d.currentDilemmaIndex < d.values.length - 1 ? d.currentDilemmaIndex + 1 : 0 })),
  }
}

export const time = writable(12)
export const play = writable(false)
export const dilemmas = createDilemmas()
export const currentDilemma = derived(dilemmas, ($dilemmas) => $dilemmas.values[$dilemmas.currentDilemmaIndex])
