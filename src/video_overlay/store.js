import { readable, writable, derived } from "svelte/store"
let twitch = window.Twitch ? window.Twitch.ext : null
if (!twitch) console.error("No twitch client found.")

// Listen for new message from twitch pubsub
const message = readable({ play: false, dilemma: { id: null, question: "", leftPosition: {}, rightPosition: {} } }, (set) => {
  twitch.listen("broadcast", (t, c, m) => {
    const parsed = JSON.parse(m)
    set({ ...parsed.message, received: new Date() })
  })
  return () => null
})

export const play = derived(message, ($message) => (!!$message.play ? $message.play : false))
export const dilemma = derived(message, ($message) => (!!$message.dilemma ? $message.dilemma : {}))
export const time = derived(message, ($message) => (!!$message.time ? $message.time : 0))
export const received = derived(message, ($message) => (!!$message.received ? $message.received : null))
export const user = writable({})
