import firebase from "firebase"
require("firebase/firestore")
firebase.initializeApp({
  apiKey: "AIzaSyBGk9DbKk14ElUSk4mzsH_AruNsl5-0Ay8",
  authDomain: "would-you-rather-e0ab2.firebaseapp.com",
  databaseURL: "https://would-you-rather-e0ab2.firebaseio.com",
  projectId: "would-you-rather-e0ab2",
  storageBucket: "would-you-rather-e0ab2.appspot.com",
  messagingSenderId: "846639797355",
  appId: "1:846639797355:web:4f114b838e29c3a1d94d2e",
  measurementId: "G-ZWZESZEJ5L",
})
firebase.analytics()
export let db = firebase.firestore()
