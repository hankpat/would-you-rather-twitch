## Environment Variables

> See https://firebase.google.com/docs/functions/config-env
> NOTE: Only lowercase keys are allowed

1. Set each of the configurations defined in the `env.yaml` one by one like

```
firebase functions:config:set tolson.ext_secret
```

2. Ensure that you set them properly by running

```
firebase functions:config:get
```
