import * as functions from "firebase-functions"
import { onStanceSelection, getRandomDebate } from "./handlers"
import { cors, checkToken } from "./middleware"

export const stances = functions.https.onRequest(async (request, response) => {
  cors(request, response, async () => {
    checkToken(request, response, async (decoded: any) => {
      await onStanceSelection(request, response, decoded)
      response.send("ok")
    })
  })
})

export const newDebate = functions.pubsub.schedule("every 1 minutes").onRun(async (context) => {
  await getRandomDebate()
})
