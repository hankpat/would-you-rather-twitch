import config from "./config"
import { verify } from "jsonwebtoken"

/**
 * Enables CORS.
 *
 * Set CORS headers for preflight requests
 * Allows GETs from any origin with the Content-Type header
 * and caches preflight response for 3600s
 */
export const cors = (request: any, response: any, callback: Function) => {
  response.set("Access-Control-Allow-Origin", "*")
  if (request.method === "OPTIONS") {
    // Send response to OPTIONS requests
    response.set("Access-Control-Allow-Methods", "OPTIONS, HEAD, GET, POST")
    response.set("Access-Control-Allow-Headers", "Content-Type, Authorization")
    response.set("Access-Control-Max-Age", "3600")
    response.status(204).send("")
  } else {
    callback()
  }
}

/**
 * Check the JWT on the request for validity.
 *
 * Middleware that precedes inbound api calls, checks for a an authorization
 * header and attempts to validate it's JWT with the configured signature.
 */
export const checkToken = (request: any, response: any, callback: Function) => {
  let token = request.headers["x-access-token"] || request.headers["authorization"] // Express headers are auto converted to lowercase
  if (token.startsWith("Bearer ")) {
    // Remove Bearer from string
    token = token.slice(7, token.length)
  }

  if (token) {
    verify(token, config.twitchSecret, (err: any, decoded: any) => {
      if (err) {
        response.status(400).send("Invalid JWT")
      } else {
        return callback(decoded)
      }
    })
  } else {
    response.status(400).send("Missing JWT")
  }
}
