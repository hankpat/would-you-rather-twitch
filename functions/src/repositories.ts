import * as admin from "firebase-admin"
const serviceAccount = require("./twitch-tolson-firebase-adminsdk-kxolm-e490c3ad5d.json")

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://twitch-tolson.firebaseio.com",
})

// Set a stance for a debate given debater info, a position, and the debate id
export const setStanceRepo = async (userid: any, positionId: any, debateId: any) => {
  try {
    const db = admin.firestore()
    const debateRef = db.collection("debates").doc(debateId)
    const stanceId = `${userid}_${debateId}`
    const stance = {
      userid: userid,
      position: positionId,
      debate: debateRef,
    }
    const stanceRef = admin.firestore().collection("stances").doc(stanceId)
    await stanceRef.set(stance, { merge: true })
    return { id: stanceId, ...stance }
  } catch (err) {
    console.error("err", err)
    return { id: null, debate: null }
  }
}

// Get the currently running debate
// const getCurrentDebateRepo = async () => {
//   try {
//     let debates: any[] = []
//     const debatesRef = admin.firestore().collection("debates")
//     const snapshot = await debatesRef.get()
//     snapshot.forEach((doc: { id: any; data: () => any }) => {
//       debates.push({ id: doc.id, ...doc.data() })
//     })
//     return debates[0] //just get the first one for now
//   } catch (err) {
//     console.log("err", err)
//   }
// }

// Get debate stances
// const getDebateStancesRepo = async (debateId: any) => {
//   try {
//     let stances: any[] = []
//     const db = admin.firestore()
//     var debateRef = db.collection("debates").doc(debateId)
//     const stancesRef = db.collection("stances")
//     const snapshot = await stancesRef.where("debate", "==", debateRef).get()
//     snapshot.forEach((doc: { data: () => any; id: any }) => {
//       let stance = doc.data()
//       delete stance.debate // we don't need the whole debate here
//       stances.push({ id: doc.id, ...stance })
//     })
//     return stances
//   } catch (err) {
//     console.log("err", err)
//   }
// }

export const getRandomDebateRepo = async () => {
  const getFirst = (snapshot: any) => {
    const arr: any[] = []
    snapshot.forEach((doc: { id: any; data: () => any }) => {
      arr.push({ id: doc.id, ...doc.data() })
    })
    return arr[0]
  }

  try {
    const db = admin.firestore()
    const debates = db.collection("debates")
    const key = debates.doc().id
    const snapshot = await debates.where(admin.firestore.FieldPath.documentId(), ">=", key).limit(1).get()

    if (snapshot.size > 0) {
      return getFirst(snapshot)
    } else {
      const snapshot2 = await debates.where(admin.firestore.FieldPath.documentId(), "<", key).limit(1).get()
      return getFirst(snapshot2)
    }
  } catch (err) {
    console.error(err)
  }
}
