import * as functions from "firebase-functions"
const configurations = functions.config()
export default {
  twitchSecret: Buffer.from(!!configurations.tolson.ext_secret ? configurations.tolson.ext_secret : "", "base64"), //ensure secret is base64
  twitchClientId: configurations.tolson.ext_client_id,
  twitchOwnerId: configurations.tolson.ext_owner_id,
}
