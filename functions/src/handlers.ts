import { setStanceRepo, getRandomDebateRepo } from "./repositories"
import { sendBroadcast } from "./realtime"

// Get current debate
// const getCurrentDebate = async (_req: any, res: { send: (arg0: any) => void }) => {
//   const debate = await repositories.getCurrentDebate()
//   res.send(debate)
// }

// Get debate stances
// const getDebateStances = async (req: { params: { debateId: any } }, res: { send: (arg0: any) => void }) => {
//   const stances = await repositories.getDebateStances(req.params.debateId)
//   res.send(stances)
// }

export const onStanceSelection = async (request: any, _response: any, decoded: any) => {
  const { position, debate } = request.body
  const stance = await setStanceRepo(decoded["opaque_user_id"], position, debate)
  delete stance.debate
  await sendBroadcast(JSON.stringify({ type: "STANCE_UPDATE_V2", payload: { ...stance }, version: "0.1.0" }))
}

export const getRandomDebate = async () => {
  const debate = await getRandomDebateRepo()
  await sendBroadcast(JSON.stringify({ type: "NEW_DEBATE_V2", payload: { ...debate }, version: "0.1.0" }))
}
