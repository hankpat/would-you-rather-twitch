import fetch from "node-fetch"
import { sign } from "jsonwebtoken"
import config from "./config"
const serverTokenDurationSec = 30000

/**
 * Broadcast a message to all the connected twitch clients.
 */
export const sendBroadcast = async (msg: any) => {
  // Set the HTTP headers required by the Twitch API.
  const headers = {
    "Client-ID": config.twitchClientId || "",
    "Content-Type": "application/json",
    Authorization: "Bearer " + makeServerToken(),
  }

  // Create the POST body for the Twitch API request.
  const body = {
    content_type: "application/json",
    message: msg,
    targets: ["global"],
  }
  // Send the broadcast request to the Twitch API.
  await fetch("https://api.twitch.tv/extensions/message/all", {
    method: "post",
    body: JSON.stringify(body),
    headers,
  })
}

/**
 * Creates a temporary JWT for access of the twitch api.
 */
function makeServerToken() {
  const payload = {
    exp: Math.floor(Date.now() / 1000) + serverTokenDurationSec,
    channel_id: "all", // for now
    user_id: config.twitchOwnerId, // extension owner ID for the call to Twitch PubSub
    role: "external",
    pubsub_perms: {
      send: ["global"],
    },
  }
  return sign(payload, config.twitchSecret, { algorithm: "HS256" })
}

module.exports = {
  sendBroadcast: sendBroadcast,
}
