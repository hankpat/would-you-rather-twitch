const fs = require("fs")
const csv = require("csv-parser")
const uuidv1 = require("uuid/v1")

var admin = require("firebase-admin")
var serviceAccount = require("../services/config/twitch-tolson-firebase-adminsdk-kxolm-e490c3ad5d.json")

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://twitch-tolson.firebaseio.com"
})

const db = admin.firestore()

// Get the csv
let count = 0,
  failed = 0
const filePath = process.argv[2]
fs.createReadStream(filePath)
  .pipe(csv())
  .on("data", async data => {
    try {
      await db
        .collection("debates")
        .add({
          positions: [
            {
              id: uuidv1(),
              orientation: 0,
              title: data.this
            },
            {
              id: uuidv1(),
              orientation: 1,
              title: data.that
            }
          ]
        })
        .then(() => {
          count += 1
        })
    } catch (err) {
      console.error(err)
      failed += 1
    }
  })
  .on("end", () => {
    console.log(`Done: Uploaded ${count} debates. ${failed} failures.`)
  })
